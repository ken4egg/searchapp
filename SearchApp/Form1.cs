﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SearchApp
{

    public partial class Form1 : Form
    {
        config ini = new config("config.ini");
        public Form1()
        {
            InitializeComponent();
            readSettings();
        }
        // Загрузка настроек
        private void readSettings()
        {
            if (ini.KeyExists("Settings", "Startfolder"))
            {
                textBox1.Text = ini.ReadINI("Settings", "Startfolder");
            }
            if (ini.KeyExists("Settings", "Filename"))
            {
                textBox2.Text = ini.ReadINI("Settings", "Filename");
            }
            if (ini.KeyExists("Settings", "Text"))
            {
                textBox3.Text = ini.ReadINI("Settings", "Text");
            }
        }

        private Thread timerThread;
        private searchclass Search;
        Stopwatch sw;
        // Формирование дерева
        private void AddToTree(TreeView treeView, string path)
        {
            var startdir = this.getParams()[0];
            var rootDirectoryInfo = new DirectoryInfo(path);
            var paths = new List<string>() { rootDirectoryInfo.Name };
            while (rootDirectoryInfo.FullName != startdir)
            {
                rootDirectoryInfo = rootDirectoryInfo.Parent;
                paths.Add(rootDirectoryInfo.Name);
            }
            paths.Reverse();
            paths.RemoveAt(0);
            TreeNode last = null;
            var key = string.Empty;
            foreach (string elem in paths)
            {
                key += elem;
                TreeNode[] nodes = treeView.Nodes.Find(key, true);
                if (nodes.Length == 0)
                {
                    if (last == null)
                    {
                        last = treeView.Nodes.Add(key, elem, 0);

                    }else
                    {
                        last = last.Nodes.Add(key, elem, 0);
                    }
                }else 
                {
                    last = nodes[0];
                }                  
                    
            }
        }
        // Выбор папки для начала поиска 
        private void textBox1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        /* геттер параметров 
            [0] - начало поиска 
            [1] - маска файла 
            [2] - текст в файле 
        */
        public string[] getParams()
        {
            return new string[] { this.textBox1.Text, this.textBox2.Text, this.textBox3.Text };
        }

        // Старт
        private void startbutton_Click(object sender, EventArgs e)
        {
            try
            {
                sw = new Stopwatch();
                sw.Start();
                timerThread = new Thread(timer_Tick);
                Search = new searchclass(this.getParams());
                Search.onUpdateProgress += (text) => {
                    message.Invoke((MethodInvoker)delegate { message.Text = text; });
                };
                Search.onFind += filepath => {
                    treeView1.Invoke((MethodInvoker)delegate { AddToTree(this.treeView1, filepath); });
                };
                Search.Finished += (count) => {
                    this.RefreshForm();
                };
                Search.StartSearch();
                startbutton.Visible = false;
                timerThread.Start();
                stopbutton.Enabled = true;
                pausebutton.Visible = true;

            }
            catch (Exception err)
            {
               MessageBox.Show(err.Message, "Exception caught.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        // Метод обновления секундомера
        private void timer_Tick()
        {
            while(Search.isWorking())
            {

                timelabel.Invoke((MethodInvoker)delegate {
                    timelabel.Text = "Elapsed time: " + sw.Elapsed.TotalSeconds.ToString() + " sec";
                });

            }
        }
        private void RefreshForm()
        {
            startbutton.Invoke((MethodInvoker) delegate { startbutton.Visible = true; });
            stopbutton.Invoke((MethodInvoker) delegate { stopbutton.Enabled = false; });
            resumebutton.Invoke((MethodInvoker) delegate { resumebutton.Visible = false; });
            pausebutton.Invoke((MethodInvoker) delegate { pausebutton.Visible = false; });
            clearbutton.Invoke((MethodInvoker) delegate { clearbutton.Visible = true; });
        }

        // Остановка поиска
        private void stopbutton_Click(object sender, EventArgs e)
        {
            
            Search.Stop();
            this.treeView1.Nodes.Clear();
            startbutton.Visible = true;
            pausebutton.Visible = false;
            resumebutton.Visible = false;
            stopbutton.Enabled = false;
            if (timerThread.ThreadState != System.Threading.ThreadState.Suspended)
            {
                timerThread.Abort();
            }
            else timerThread = null;
            timelabel.Text = "Elapsed time: ";
        }
        // Пауза
        private void button1_Click(object sender, EventArgs e)
        {
            Search.Pause();
            sw.Stop();
#pragma warning disable CS0618 // Type or member is obsolete
            timerThread.Suspend();
#pragma warning restore CS0618 // Type or member is obsolete
            pausebutton.Visible = false;
            resumebutton.Visible = true;
        }
        // Возобновление
        private void resumebutton_Click(object sender, EventArgs e)
        {
            Search.Resume();
            sw.Start();
#pragma warning disable CS0618 // Type or member is obsolete
            timerThread.Resume();
#pragma warning restore CS0618 // Type or member is obsolete
            resumebutton.Visible = false;
            pausebutton.Visible = true;
        }
        // Завершение потоков, если нажали на закрыть во время поиска + сохранение настроек
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            ini.Write("Settings", "Startfolder", textBox1.Text);
            ini.Write("Settings", "Filename", textBox2.Text);
            ini.Write("Settings", "Text", textBox3.Text);

            if (timerThread != null)
            {
                if(timerThread.IsAlive)
                {
                    timerThread.Abort();
                }
            }

            if(Search!=null)
            {
                if (Search.isWorking())
                {
                    Search.Stop();
                }
            }
            System.Environment.Exit(1);
        }

        private void clearbutton_Click(object sender, EventArgs e)
        {
            RefreshForm();
            timelabel.Text = "Elapsed time: ";
            message.Text = "";
            this.treeView1.Nodes.Clear();
            clearbutton.Visible = false;
        }
    }

}
