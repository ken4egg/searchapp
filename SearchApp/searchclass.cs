﻿using System.IO;
using System.Threading;

namespace SearchApp
{
    public delegate void UpdateProgress(string text);
    public delegate void Find(string filepath);
    public delegate void Finished(int count);
    class searchclass
    {
        private string[] param;
        public searchclass(string[] _param) {

            if (!Directory.Exists(_param[0])) throw new System.ArgumentException("Wrong start folder");
            if (_param[1].Trim()=="") throw new System.ArgumentException("Wrong filename pattern");
            if (_param[2].Trim() == "") throw new System.ArgumentException("Wrong text");
            this.param = _param;
        }
        public event UpdateProgress onUpdateProgress;
        public event Find onFind;
        public event Finished Finished;
        private bool Working = false;
        private int count = 0;
        private Thread thread;
        public bool isWorking()
        {
            return this.Working;
        }
        // Старт поиска
        public void StartSearch()
        {
            this.Working = true;
            thread = new Thread(Searching);
            thread.Start();
        }
        public void Stop()
        {
            if(thread.ThreadState != ThreadState.Suspended)
            {
                thread.Abort();
            }else thread = null;
            onUpdateProgress?.Invoke("");
            this.count = 0;
            this.Working = false;
        }
        public void Pause()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            thread.Suspend();
#pragma warning restore CS0618 // Type or member is obsolete
        }
        public void Resume()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            thread.Resume();
#pragma warning restore CS0618 // Type or member is obsolete
        }

        // Сам поиск
        public void Searching()
        {
            DirectoryInfo di = ListText(param[0]);
            foreach (FileInfo file in di.EnumerateFiles(param[1], SearchOption.AllDirectories))
            {
                onUpdateProgress?.Invoke("Current file: " + file.FullName + "\nprocessed " + this.count++ + " files.");
                string str = File.ReadAllText(file.FullName).ToLower();
                if (str.Contains(param[2].Trim().ToLower()))
                {
                    onFind?.Invoke(file.FullName);
                }
            }
            onUpdateProgress?.Invoke("Processed " + this.count + " files");
            Finished?.Invoke(this.count);
            this.Working = false;
            this.count = 0;
        }
        // Получение списка файлов в директории
        public DirectoryInfo ListText(string DirName = @"c:\temp\")
        {
            DirectoryInfo di = new DirectoryInfo(DirName);
            return di;
        }
    }

}
